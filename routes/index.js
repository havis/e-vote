var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
}),
router.get('/formlogin', function(req, res, next) {
  res.render('formlogin', { title: 'Express' });
});
router.get('/pemilihan', function(req, res, next) {
  res.render('pemilihan', { title: 'Express' });
});
router.get('/calon-ketua', function(req, res, next) {
  res.render('calon-ketua', { title: 'Express' });
});


module.exports = router;
